MTSEdit User Manual
===================

This little tool can be compiled on any system easily. It's only dependency is SDL2. Without an argument, or with `-h` it prints
an usage to stdout.

See also [batch mode usage](https://gitlab.com/bztsrc/mtsedit/blob/master/docs/batch.md).

```
MineTest Schematics Editor by bzt Copyright (C) 2019 MIT license

./mtsedit [-v] [-l <lang>] <.mts|.schematic|directory>
```

Configuration
-------------

For the data directory, see [blocks.csv](https://gitlab.com/bztsrc/mtsedit/blob/master/docs/blocks.md). If not found, provide
the path in MTSDATA environment variable.

The User Interface is translatable. It should detect your computer's language automatically, but if not, set the `LANG`
environment variable, or add `-l` to command line (useful in .desktop and .lnk files). Supported languages:
 - "en" English
 - "hu" Magyar
 - "es" Español
 - "fr" Français
 - "de" Deutch
 - "pl" Polski
 - "ru" Русский
 - "pt" Português
 - "it" Italiano

```
LANG=hu ./mtsedit monkeyisland.mts
./mtsedit -l hu monkeyisland.mts
```

Note that block names are user provided, there's no translation for those, unless you provide `blocks_(lang).txt` files too.

Interactive Mode
----------------

This is the default. If you start MTSEdit with a single parameter, being an MTS file's path, it will open it in the GUI editor.
If that parameter is a directory, then MTSEdit will start with the "Load Schematic from File" screen.

### Main Editor Window

<img src="https://gitlab.com/bztsrc/mtsedit/raw/master/docs/mtseditdoc.png">

You can find the toolbar `A` on the left. The icons are:
1. Load Structure
2. Save As
3. Save Preview
4. Layer Operations (small icons)
5. Undo / Redo (last two small icons)
6. Select Brush
7. Add Blocks to Palette

Layer Operations are: change orientation, flip structure, insert Y layer, remove Y layer, insert X layer, remove X layer,
insert Z layer, remove Z layer.

Beneath the icons is the palette `B`. You can add blocks to here from the Add Blocks menu, and then you can quickly access them
by shortcuts.

On the bottom left corner, you find the Layer-related options `C`:

1. Layer Up
2. Layer Down
3. Set Ground Level
4. Current Layer
5. Current Layer's Probability

The bottom line is the status bar `D`. You'll see the block names here as you hover over them. Messages like file saved also shown
here. On the right, you see:

1. current block's rotation (00 - 1F)
2. current block's force placement indicator icon
3. the current zoom ratio, and
4. the brush's height and shape.

Everything else on the screen is the main editor area `E`.

### Mouse Handling

| Mouse Event                       | Description                                       |
|-----------------------------------|---------------------------------------------------|
| mouse left button                 | place the selected block                          |
| mouse right button                | rotate block                                      |
| middle buttons (wheel)            | scroll the editor area                            |
| <kbd>Shift</kbd> + wheel up/down  | move the upper layers up and down                 |
| <kbd>Shift</kbd> + wheel left     | rotate the current block counter clock-wise       |
| <kbd>Shift</kbd> + wheel right    | rotate the current block clock-wise               |
| <kbd>Ctrl</kbd> + wheel up/down   | zoom in or out                                    |

### Keyboard Shortcuts

The GUI was designed in a way that you can use it without a mouse (actually it's faster to only use the keyboard in some cases).
Therefore the keyboard shortcuts may seem odd; the reason for the layout is that <kbd>Space</kbd> is the main "painter" key, and
the other functions are arranged close to it depending how often they are needed. Having a key of the letter that the function
starts with was a secondary importance.

Main editor window:

| Key                               | Description                                       |
|-----------------------------------|---------------------------------------------------|
| <kbd>PgUp</kbd> / <kbd>PgDn</kbd> | change current layer                              |
| arrow keys                        | move cursor, select current block                 |
| <kbd>Space</kbd>                  | place selected block                              |
| <kbd>Backspace</kbd>              | erase current block                               |
| <kbd>Enter</kbd>                  | flood fill (or erase) entire areas                |
| <kbd>Ctrl</kbd> + arrow keys      | scroll the editor area                            |
| <kbd>Shift</kbd> + up / down      | move the upper layers up and down                 |
| <kbd>Shift</kbd> + left           | rotate the current block counter clock-wise       |
| <kbd>Shift</kbd> + right          | rotate the current block clock-wise               |
| <kbd>+</kbd> / <kbd>-</kbd>       | change current layer's probability                |
| <kbd>L</kbd>                      | open load schematic window                        |
| <kbd>S</kbd>                      | quick save schematic to current file              |
| <kbd>Shift</kbd> + <kbd>S</kbd>   | open save as window                               |
| <kbd>P</kbd>                      | save preview PNG                                  |
| <kbd>Shift</kbd> + <kbd>P</kbd>   | save preview with structure cut in half           |
| <kbd>E</kbd>                      | export blueprint PNG                              |
| <kbd>Shift</kbd> + <kbd>E</kbd>   | export plain blueprint without legend             |
| <kbd>R</kbd>                      | change orientation, rotate entire structure CCW   |
| <kbd>Shift</kbd> + <kbd>R</kbd>   | change orientation, rotate entire structure CW    |
| <kbd>F</kbd>                      | flip the entire structure along the Z axis        |
| <kbd>Y</kbd>                      | redo node placement from history                  |
| <kbd>Z</kbd>                      | undo last node placement / erase                  |
| <kbd>X</kbd>                      | insert a new, emtpy Y layer                       |
| <kbd>Shift</kbd> + <kbd>X</kbd>   | duplicate current Y layer                         |
| <kbd>Ctrl</kbd> + <kbd>X</kbd>    | remove current Y layer                            |
| <kbd>C</kbd>                      | insert a new, empty X layer                       |
| <kbd>Shift</kbd> + <kbd>C</kbd>   | duplicate current X layer                         |
| <kbd>Ctrl</kbd> + <kbd>C</kbd>    | remove current X layer                            |
| <kbd>V</kbd>                      | insert a new , empty Z layer                      |
| <kbd>Shift</kbd> + <kbd>V</kbd>   | duplicate current Z layer                         |
| <kbd>Ctrl</kbd> + <kbd>V</kbd>    | remove current Z layer                            |
| <kbd>B</kbd>                      | select brush (use geometric objects)              |
| <kbd>N</kbd>                      | pick node                                         |
| <kbd>A</kbd>                      | toggle show all / show current layer only         |
| <kbd>M</kbd> / <kbd>Tab</kbd>     | open block map, search all available blocks       |
| <kbd>;</kbd>                      | toggle grid                                       |
| <kbd>,</kbd> / <kbd>&lt;</kbd>    | zoom out                                          |
| <kbd>.</kbd> / <kbd>&gt;</kbd>    | zoom in                                           |
| <kbd>0</kbd>, <kbd>Backquote</kbd> | select Air block (erase mode)                    |
| <kbd>1</kbd> - <kbd>9</kbd>       | quickly access one of the blocks from the palette |
| <kbd>G</kbd>                      | set current layer as ground level                 |
| <kbd>D</kbd>                      | toggle force placement flag                       |
| <kbd>Q</kbd> / <kbd>Esc</kbd>     | quit MTSEdit                                      |

Add blocks to palette window:

| Key                               | Description                                       |
|-----------------------------------|---------------------------------------------------|
| any key                           | instant search blocks                             |
| <kbd>Ctrl</kbd> + number          | add the Nth search result to the palette          |
| <kbd>Tab</kbd> / <kbd>Esc</kbd>   | return to main editor window                      |
